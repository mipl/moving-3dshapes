import numpy as np
import os, wget, h5py, math
from numpy.random import randint

class ThreeDShapes:

    '''

    Most of the code in __init__() and get_index() was adapted from the official
    repository of Deepmind's 3dShapes dataset, under Apache License 2.0, in
    order to retrieve the metadata and accessing images through indexing.

    https://github.com/deepmind/3d-shapes

    '''

    def __init__(self):

        if not os.path.exists('./3dshapes.h5'):
            url = 'https://storage.googleapis.com/3d-shapes/3dshapes.h5'
            print('3dshapes.h5 file not found. Downloading from ' + url)
            wget.download(url, './')

        print('Loading images into memory...')

        dataset = h5py.File('./3dshapes.h5', 'r')

        self.images = dataset['images'][()]
        self.labels = dataset['labels']
        self.image_shape = self.images.shape[1:]
        self.label_shape = self.labels.shape[1:]
        self.n_samples = self.labels.shape[0]

        self._FACTORS_IN_ORDER = ['floor_hue', 'wall_hue', 'object_hue',
                                  'scale', 'shape', 'orientation']

        self._NUM_VALUES_PER_FACTOR = {'floor_hue': 10, 'wall_hue': 10,
                                       'object_hue': 10, 'scale': 8, 'shape': 4,
                                       'orientation': 15}

    def get_index(self, factors):
        """ Converts factors to indices in range(num_data)
        Args:
        factors: np array shape [6,batch_size].
                 factors[i]=factors[i,:] takes integer values in 
                 range(_NUM_VALUES_PER_FACTOR[_FACTORS_IN_ORDER[i]]).

        Returns:
        indices: np array shape [batch_size].
        """
        indices = 0
        base = 1
        for factor, name in reversed(list(enumerate(self._FACTORS_IN_ORDER))):
            indices += factors[factor] * base
            base *= self._NUM_VALUES_PER_FACTOR[name]
        return indices

    def sample_factors(self, force_contrast = True, fixed = {}):

        if force_contrast:

            # Ensure that the colors of the object, the floor and the walls are diffrent.

            hue_pool = list(range(10))

            def pop_fixed(f):
                if f not in fixed:
                    return None
                if fixed[f] in hue_pool:
                    del hue_pool[hue_pool.index(fixed[f])]
                return fixed[f]

            def pop_random(f):
                if f is not None:
                    return f
                hue = np.random.choice(hue_pool)
                del hue_pool[hue_pool.index(hue)]
                return hue

            floor_hue  = pop_fixed('floor_hue')
            wall_hue   = pop_fixed('wall_hue')
            object_hue = pop_fixed('object_hue')

            floor_hue  = pop_random(floor_hue)
            wall_hue   = pop_random(wall_hue)
            object_hue = pop_random(object_hue)
        else:
            floor_hue  = fixed['floor_hue']  if 'floor_hue'  in fixed else randint(0, 10)
            wall_hue   = fixed['wall_hue']   if 'wall_hue'   in fixed else randint(0, 10)
            object_hue = fixed['object_hue'] if 'object_hue' in fixed else randint(0, 10)

        s0    = fixed[  's0' ] if   's0'  in fixed else randint(0, 8)
        sf    = fixed[  'sf' ] if   'sf'  in fixed else randint(0, 8)
        p0    = fixed[  'p0' ] if   'p0'  in fixed else randint(0, 15)
        pf    = fixed[  'pf' ] if   'pf'  in fixed else randint(0, 15)
        shape = fixed['shape'] if 'shape' in fixed else randint(0, 4)
        speed = fixed['speed'] if 'speed' in fixed else randint(1, 3)

        return (floor_hue, wall_hue, object_hue, s0, sf, shape, p0, pf, speed)

    def generate_trajectory(self, floor_hue, wall_hue, object_hue, s0, sf,
                                        shape, p0, pf, speed, min_frames = 5):
        x0, y0 = (s0, p0)
        xf, yf = (sf, pf)
        
        frames     = max(abs(sf-s0), abs(pf-p0))
        frames_f   = math.ceil((frames + 1) / speed)

        if frames_f < min_frames:
            return None

        y_step     = math.ceil(frames/(max(abs(pf-p0), 1)))
        x_step     = math.ceil(frames/(max(abs(sf-s0), 1)))
        y_step_cur = y_step
        x_step_cur = x_step
        
        x, y = (x0, y0)
        img  = [self.images[self.get_index(
                            [floor_hue, wall_hue, object_hue, x, shape, y])]]

        for _ in range(frames):
            if x_step_cur == 1:
                if abs(xf-x) > 0:
                    x += np.sign(xf-x0)
                    x_step_cur = x_step
            else:
                x_step_cur -= 1

            if y_step_cur == 1:
                if abs(yf-y) > 0:
                    y += np.sign(yf-y0)
                    y_step_cur = y_step
            else:
                y_step_cur -= 1

            img.append(self.images[self.get_index(
                [floor_hue, wall_hue, object_hue, x, shape, y])])

        img = img[::speed]
        img = np.stack(img)

        return img
