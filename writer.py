import os
import imageio
import numpy as np

def check_and_create_path(check_path):
    if not os.path.exists(check_path):
        os.makedirs(check_path)

def save_gif(vid, f_name):

    if(vid.shape[-1] == 1):
        vid = np.repeat(vid, 3, axis = -1)
    imageio.mimsave(f_name + '.gif', vid)

    return 'gif'

def save_mp4(vid, f_name):

    if(vid.shape[-1] == 1):
        vid = np.repeat(vid, 3, axis = -1)
    imageio.mimsave(f_name + '.mp4', vid, quality = 10, pixelformat = 'yuv444p')
    
    return 'mp4'

def save_png(vid, f_name):

    f, h, w, c = vid.shape

    H = int(np.sqrt(f))
    W = int(np.ceil(f / H))

    if f < H * W:
        zr = (0, 0)
        vid = np.pad(vid, [(0, H * W - f), zr, zr, zr], 
                        mode = 'constant', constant_values = 1)

    vid = np.reshape(vid, (H, W, h, w, c))
    vid = np.transpose(vid, (0, 2, 1, 3, 4))
    vid = np.reshape(vid, (H * h, W * w, c))

    imageio.imsave(f_name + '.png', vid)
    
    return 'png'

def save_pngh(vid, f_name):
    vid = np.reshape(np.transpose(vid, (0, 2, 1, 3)), (-1, 64, 3))
    vid = np.transpose(vid, (1, 0, 2))
    imageio.imsave(f_name + '.png', vid)
    return 'png'

def save_pngv(vid, f_name):
    vid = np.reshape(vid, (-1, 64, 3))
    imageio.imsave(f_name + '.png', vid)
    return 'png'

FUNCS = { 'gif' : save_gif,   'mp4' : save_mp4, 'png' : save_png,
         'pngh' : save_pngh, 'pngv' : save_pngv}

class VideoWriter:

    def __init__(self, tp):

        self.s_func = FUNCS[tp]

    def save(self, vid, out_path, f_name):

        f_name = os.path.join(out_path, f_name)
        return self.s_func(vid, f_name)

        