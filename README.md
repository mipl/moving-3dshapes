# Moving 3dShapes

The Moving 3dShapes dataset is built on the basis of the [Deepmind's 3dShapes dataset](https://github.com/deepmind/3d-shapes).

This library contains the Python code to generate videos from the images of the
3dShapes dataset, in which the motions yielded correspond to linear changes in
the scale of the shape, and the linear displacement of the perspective, giving
the sensation of camera movement.
You can control and mix these two motions.

## Factors of Variation

The generated videos has the following factors of variation:

| Symbol     | Description                      | Possible values (inclusive ranges) |
| ---------- | -------------------------------- |:----------------------------------:|
| floor_hue  | Floor hue (as original dataset)  | 0-9                                |
| wall_hue   | Wall hue (as original dataset)   | 0-9                                |
| object_hue | Object hue (as original dataset) | 0-9                                |
| s0         | Initial size                     | 0-7                                |
| sf         | Final size                       | 0-7                                |
| p0         | Initial perspective              | 0-14                               |
| pf         | Final perspective                | 0-14                               |
| shape      | Shape (as original dataset)      | 0-3                                |
| speed      | Speed of motion                  | 1-2                                |


Most the floor_hue, wall_hue, object_hue and shape factors may result familiar
to the ones acquainted to the original dataset.
The initial and final sizes and perspectives correspond to the size and
perspective factors in the original dataset, and the generated videos consist
of a the images that fit inside a linear trajectory between the initial and
final size, and the initial and final perspectives.

## Formats

You can choose between mp4, GIF and PNG format to store the videos. The PNG
format corresponds to mosaic images where the frames are in either horizontal,
vertical or matrix (default) tiles, as shown in the examples bellow.

PNG

Creates a matrix mosaic of $`H \times W`$ where

```python
# f : number of frames
H = int(np.sqrt(f))
W = int(np.ceil(f / H))
```

<img src="examples/0_2_4_5_7_2_0_14_1.png">

PNG horizontal

<img src="examples/0_2_4_5_7_2_0_14_1_h.png">

PNG vertical

<img src="examples/0_2_4_5_7_2_0_14_1_v.png">

GIF

<img src="examples/0_2_4_5_7_2_0_14_1.gif">

mp4

<a href="examples/0_2_4_5_7_2_0_14_1.mp4">Example</a>

## Dependencies

Before running the code, you must have the `imageio`, `numpy`, `argparse`,
`tqdm`, `h5py`, `wget`, and `imageio-ffmpeg` Python libraries.

```
$ pip install -r requirements.txt
```

## Usage

```sh
$ python main.py --help
usage: main.py [-h] [-n NUM_EXAMPLES] [-f MIN_FRAMES] [-s RANDOM_STATE]
               [-r {gif,mp4,png,pngh,pngv}] [-c]
               output_path

Generation of the Moving-3dShapes dataset

positional arguments:
  output_path           Output path.

optional arguments:
  -h, --help            show this help message and exit
  -n NUM_EXAMPLES, --num_examples NUM_EXAMPLES
                        Number of videos to be generated.
  -f MIN_FRAMES, --min_frames MIN_FRAMES
                        Minimum number of frames per video.
  -s RANDOM_STATE, --random_state RANDOM_STATE
                        Random state.
  -r {gif,mp4,png,pngh,pngv}, --format {gif,mp4,png,pngh,pngv}
                        Format of the output videos (png format are images
                        whose frames are in an horizontal, vertical or matrix
                        tile).
  -c, --force_contrast  Whether to force constrast between the object, the
                        floor and the wall.
```

Examples:

```sh
$ python main.py ./vids/           # To produce 10000 PNG matrix videos in ./vids
$ python main.py ./vids/ -r pngh   # To produce 10000 PNG horizontal videos in ./vids
$ python main.py ./vids/ -r gif -c # To produce 10000 GIFs videos where the colors of the object, the floor and the walls are forced to be different.
```

Check the `sample_factors` function at `three_d_shapes.py` in order to see how to
personalize the generation process, by fixing the factors you want.

### List of Factors

A file named `list` will also be generated, in order to associate each video to
its factors of variation and number of frames. Each line in the file has the
following format:

```
<file name> <num_frames> <floor_hue> <wall_hue> <object_hue> <s0> <sf> <shape> <p0> <pf> <speed>
```

## Contributors

The trajectory generator was developed by Arthur Costa Lopes, and the interface and video writers was developed by Juan Hernández Albarracín. Both contributors are from University of Campinas, Brazil.